<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Book;
use App\Author;
use Carbon\Carbon;

class BookController extends Controller
{
    public function external () 
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get('https://www.anapioficeandfire.com/api/books');
        $body=  $res->getBody();
        $contents = json_decode($body);
        
        foreach ($contents as $content){
            foreach($content->authors as $author){
                $author;
            }

            $data[] = [
                'name'=> $content->name,
                'isbn'=> $content->isbn,
                'author'=> [$author],
                'number_of_pages'=> $content->numberOfPages,
                'publisher'=> $content->publisher,
                'country'=> $content->country,
                'released_date'=> $content->released,
            ] ;

        }
        $response = [
            'status_code' => 200,
            'status' => 'success',
            'data' => $data
        ];

        return response()->json($response, 200);
    }
  
    
    public function store(Request $request)
    {
        $input = $request->all();
        
        $input["release_date"] = Carbon::now();
        $book = Book::create($input);
        $data = $book->toArray();

        $response = [
            'status_code' => 200,
            'status' => 'success',
            'data' => $data
        ];

        return response()->json($response, 200);
    }
    
    public function index()
    {
        $books = Book::all();
        foreach ($books as $book){
            $book["authors"] = explode(',' , $book["authors"]);
        }

        $response = [
            'status_code' => 200,
            'status' => 'success',
            'data' => $book
        ];

        return response()->json($response, 200);
    }
    
    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        $input = $request->all();
        $book->update($input);

        $response = [
            'status_code' => 200,
            'status' => 'success',
            'message' => 'The book '.$book->name.' was updated successfully',
            'data' => $book
        ];

        return response()->json($response, 200);
    }

    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();

        $response = [
            'status_code' => 204,
            'status' => 'success',
            'data' => $book,
            'message' => 'The book '.$book->name.' was deleted successfully'
        ];

        return response()->json($response, 200);
    }

    public function show($id)
    {
        $book = Book::find($id);
        $book["authors"] = explode(',' , $book["authors"]);

        $response = [
            'status_code' => 200,
            'status' => 'success',
            'data' => $book,
            'message' => 'Book retrieved successfully.'
        ];

        return response()->json($response, 200);
    }
    
}
