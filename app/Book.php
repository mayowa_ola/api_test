<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['name','authors' ,'isbn', 'number_of_pages', 'publisher', 'country'];
    
    protected $casts = [
        'authors' => 'array',
    ];
        
}
