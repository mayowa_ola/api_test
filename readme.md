
## Api Assessment

This is a short api assessment which implement a RESTful API that calls an external API service to get information about books. IIt also implements a simple CRUD (Create, Read, Update, Delete) API with a local database.
The API Assesment was developed using Laravel 5.8 and the local database runs on MYSQL


## Getting Started
To Test the api, you can clone the repository to your local computer.

## Prerequisites
To test the api install **[composer](https://getcomposer.org)** if you dont have composer installed on your local machine.
Ensure you have php version > 7.03 and mysql

## Installing
Run Composer Install to install the packages which the application depends on.
After whcih you create your** environmental variable .env file and enter your variables in the file.
Then run the following commands
- php artisan key:generate; generates the application key
- php artisan migrate; migrates the database on your local machine 
- php artisan seeds; seeds data into the database

## Testing

To test the assesment you can use **[Post Man](https://www.getpostman.com/)** to test the various CRUD operation


## Builtwith

**[Laravel 5](https://laravel.com/)**

## Author

**[Mayowa Oladunjoye](https://bitbucket.org/mayowa_ola/)**